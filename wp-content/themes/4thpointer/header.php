<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>

    <!----------------------------------Meta Tags------------------------------------------->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="4thpointer Services was established in the year 2012 based in Delhi, NCR India to provide cost effective, 
    reliable and quality in IT related business solutions, web portals and software’s, which enable our customers to have sustainable differential 
    advantage over their competition, to improve operational efficiency, enhance productivity, effectively combat competition, enlarge market share
     and to facilitate overall growth." />
    <meta name="keywords" content="Best Website Development Company in Delhi India, Responsive Web Designing, Dynamic Website 
    Development, Custom Enterprise Application Development, Toys Renting Software, Search Engine Optimization Service, Social Media Marketing, Facebook Promotion, QA and Testing Services, Promotional Videos Development, 
     Domain Registration & Hosting, Flip Book Portfolio Designing, Outsourcing ASP.NET development,  Online Accounting Software, Mobile Application
      Development, Content Management System, Website Designing, Website Maintenance, E-Commerce Solution, Training Services" />
    <meta name="abstract" content="Web solution provider company in india - Responsive Web Designing, E-Learning Web Solutions, Logistics Busines Solutions Provider, Digital marketing and promotions, Industrial and Corporate Trainings" />
    <meta name="country" content="India" />
    <meta name="author" content="4thPointer Services" />
    <meta name="organization-Email" content="info@4thpointer.com" />
    <meta name="copyright" content="4thPointer Services" />
    <meta name="coverage" content="Worldwide" />
    <meta name="revisit_after" content="7days" />
    <meta name="language" content="English" />
	<!-- facebook graph share properties -->
    <meta property="og:title" content="4thPointer Services">
	<meta property="og:url" content="http://4thpointer.com/">
	<meta property="og:description" content="4thpointer Services was established in the year 2012 based in Delhi, NCR India to provide cost effective, 
    reliable and quality in IT related business solutions, web portals and software’s, which enable our customers to have sustainable differential 
    advantage over their competition, to improve operational efficiency, enhance productivity, effectively combat competition, enlarge market share
     and to facilitate overall growth.">
	<meta property="og:image" content="http://4thpointer.com/beta/img/4thpointer.jpg">
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
	
	<!-----------------------------------------Stylesheet---------------------------------->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
    <!-- Responsive -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" rel="stylesheet">
    <!-- Choose Layout -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet" media="screen">
    <!-- Choose Skin -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/skin-blue.css" rel="stylesheet" />
    <!--<link href="css/skin-red.css" rel="stylesheet">-->
    <!-- Demo -->
    <link rel="stylesheet" id="main-color" href="<?php echo get_template_directory_uri(); ?>/css/skin-blue.css" media="screen"/>
    <!--<link rel="stylesheet" id="main-color" href="css/skin-red.css" media="screen"/>-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico">
    <!-- IE -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>	   
    <![endif]-->
    <!--[if lte IE 8]>
	<link href="css/ie8.css" rel="stylesheet">
	 <![endif]-->
	
	
	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body class="off">
  <!-- /.wrapbox start-->
  <div class="wrapbox">
	<!-- TOP AREA === -->
	<section class="toparea">
	<div class="container">
		<div class="row">
			<div class="col-md-6 top-text pull-left animated fadeInLeft">
				<i class="icon-phone"></i> Phone: +91-8882715488, 8750115488 <span class="separator"></span><i class="icon-envelope"></i> Email: <a href="mailto:info@4thpointer.com">info@4thpointer.com</a>
			</div>
			<div class="col-md-6 text-right animated fadeInRight">
				<div class="social-icons">
					<a class="icon icon-facebook" href="http://www.facebook.com/4thPointer" target="_blank" title="Facebook"></a>
					<a class="icon icon-twitter" href="https://twitter.com/#!/4thPointer" target="_blank" title="Twitter"></a>
					<a class="icon icon-linkedin" href="https://www.linkedin.com/company/4thpointer-services?trk=tyah&trkInfo=clickedVertical%3Acompany%2Cidx%3A2-1-2%2CtarId%3A1431552111347%2Ctas%3A4thpointer" target="_blank" title="Linkedin"></a>
				</div>
			</div>
		</div>
	</div>
	</section>
	<!-- /.toparea end-->
	<!-- NAV === -->
	<nav class="navbar navbar-fixed-top wowmenu" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand logo-nav custom-logo" href="index.php"><img src="img/4thpointer_logo.png" alt="logo"></a>
		</div>
		<ul id="nav" class="nav navbar-nav pull-right">
			<li class="active"><a href="index.php">Home</a></li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Company</a>
				<ul class="dropdown-menu ul-width">
					<li><a href="about_4thpointer.php">About 4thPointer</a></li>
					<li><a href="why_choose_us.php">Why Choose Us</a></li>
					<li><a href="our-alliances.php">Our Alliances</a></li>
					<li><a href="careers.php">Career</a></li>
				</ul>
			</li>
            <li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Services</a>
				<ul class="dropdown-menu ul-width">
					<li><a href="responsive_web_designing.php">Responsive Web Designing</a></li>
					<li><a href="business_solution_and_support.php">Business Applications</a></li>
					<li><a href="digital_marketing.php">Digital Marketing</a></li>
					<li><a href="training_and_innovation.php">Training and Innovation</a></li>
					<li><a href="app_development.php">Mobile App Development</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Industries</i></a>
				<ul class="dropdown-menu ul-width">
					<li><a href="logistics_and_transport.php">Logistics and Transport</a></li>
					<li><a href="e-learning.php">E-Learning </a></li>
					<!-- <li><a href="online-examination.php">Online Examination</a></li> -->
					<li><a href="real_estate.php">Real Estate </a></li>
					<li><a href="e-commerce.php">E-Commerce</a></li>
					<!-- <li><a href="Medical-and-healthcare.php">Medical and healthcare</a></li> -->
				</ul>
			</li>
			<li><a href="portfolio.php">Portfolio</a></li>
            <li><a href="contact.php">Contact Us</a></li>
		</ul>		
	</div>	
	</nav>
	<!-- /nav end-->
