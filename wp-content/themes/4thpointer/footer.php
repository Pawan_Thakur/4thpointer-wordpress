<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div><!-- .site-content -->

	<section>
	<div class="footer">
		<div class="container animated fadeInUpNow notransition">
			<div class="row">
				<div class="col-md-3">
					<h1 class="footerbrand">Get in Touch</h1>
					<p class="text-justify">4thPointer is a India born & based, IT services provider with a single focus - we deliver the highest quality service to the people that matter - our customers We offer a fully-managed service for people who value the difference service, attention & excellence can bring to their business. </p>
				</div>
				<div class="col-md-3">
					<h1 class="title"><span class="colortext">F</span>ind <span class="font100">Us</span></h1>
					<div class="footermap">
						<p>C-55, 2nd Floor, Sector 2, Noida, India, 201301</p>
						<p><strong>Phone: </strong> +91-8882715488, 8750115488</p>
						<p><strong>Email: </strong> <a href="mailto:info@4thpointer.com" style="color:#888;">info@4thpointer.com</a></p>
						<ul class="social-icons list-soc">
							<li><a href="http://www.facebook.com/4thPointer" target="_blank" title="Facebook"><i class="icon-facebook"></i></a></li>
							<li><a href="https://twitter.com/#!/4thPointer" target="_blank" title="Twitter"><i class="icon-twitter"></i></a></li>
							<li><a href="https://www.linkedin.com/company/4thpointer-services?trk=tyah&trkInfo=clickedVertical%3Acompany%2Cidx%3A2-1-2%2CtarId%3A1431552111347%2Ctas%3A4thpointer" target="_blank" title="Linkedin"><i class="icon-linkedin"></i></a></li>

						</ul>
					</div>
				</div>
				<div class="col-md-3">
					<h1 class="title"><span class="colortext">C</span>lients' <span class="font100">Voice</span></h1>
					<div id="quotes">
						<div class="textItem">
							<div class="avatar">
                                <img src="img/varun_gupta.jpg" alt="4thpointer" />
							</div>
							 4thpointer provides the best quality deliverables even in the most strenuous times. With our aggressive deadlines, the team always emerges successfully....<span style="font-family:arial;">"</span><br/><b> Mr. Varun Gupta </b><br/> - Founder of rentinandout.com
						</div>
						<div class="textItem">
							<div class="avatar">
                                <img src="img/rahul.jpg" alt="4thpointer" />
							</div>
                             "4thpointer Services showed professionalism and outstanding efficiency. Any questions and problems, arising during the development cycle....<span style="font-family:arial;">"</span><br/><b> Mr. Rahul Agarwal </b><br/> - MD of Santushti Securities Pvt. Ltd.
						</div>
                        <div class="textItem">
							<div class="avatar">
                                <img src="img/abhiskh.jpg" alt="4thpointer" />
							</div>
							 "4thpointer proposed us personnel of very high quality. I have personally interviewed a large number of people presented by 4thpointer....<span style="font-family:arial;">"</span><br/><b>Mr. Abhishek Bajaj </b><br/> - Founder of CAMistri.com
						</div>
					</div>
					<div class="clearfix">
					</div>
				</div>
				<div class="col-md-3">
					<h1 class="title"><span class="colortext">Q</span>uick <span class="font100">Links</span></h1>
					<ul class="icons chevronlist footer-link">
						<li><a href="responsive_web_designing.php" title="Responsive Web Designing">Responsive Web Designing</a></li>
						<li><a href="business_solution_and_support.php" title="Business Solution">Business Applications</a></li>
						<li><a href="digital_marketing.php" title="Digital Marketing">Digital Marketing</a></li>
						<li><a href="training_and_innovation.php" title="Training and Innovation">Training and Innovation</a></li>
						<li><a href="app_development.php" title="App Development">Mobile App Development</a></li>
						<li><a href="logistics_and_transport.php" title="Logistics and Transport">Logistics and Transport</a></li>
						<li><a href="e-learning.php" title="E-Learning">E-Learning </a></li>
						<li><a href="real_estate.php" title="Real State">Real Estate </a></li>
						<li><a href="e-commerce.php" title="E-Commerce">E-Commerce </a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<p id="back-top">
		<a href="#top"><span></span></a>
	</p>
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<p class="pull-left">&copy; Copyright 2016 by 4thpointer Services. All Rights Reserved.</p>
				</div>
				<div class="col-md-8">
					<ul class="footermenu pull-right">
                        <li><a href="index.php" title="Home">Home</a></li>
                        <li><a href="about_4thpointer.php" title="About 4thPointer">About 4thPointer</a></li>
						<li><a href="why_choose_us.php" title="Why Choose Us">Why Choose Us</a></li>
						<li><a href="our-alliances.php" title="Our Alliances">Our Alliances</a></li>
						<li><a href="careers.php" title="Careers">Career</a></li>
                        <li><a href="portfolio.php" title="Portfolio">Portfolio</a></li>
                        <li><a href="contact.php" title="Contact">Contact US</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	</section>
	</div>

</div><!-- .site -->

   <!----------------------JS-------------------------------------------->
   <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
   <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
   <script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
   <script src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>
   <script>
	/* ---------------------------------------------------------------------- */
	/*	Carousel
	/* ---------------------------------------------------------------------- */
	$(window).load(function(){			
		$('#carousel-projects').carouFredSel({
		responsive: true,
		items       : {
        width       : 200,
        height      : 295,
        visible     : {
            min         : 1,
            max         : 4
        }
    },
		width: '200px',
		height: '295px',
		auto: true,
		circular	: true,
		infinite	: false,
		prev : {
			button		: "#car_prev",
			key			: "left",
				},
		next : {
			button		: "#car_next",
			key			: "right",
					},
		swipe: {
			onMouse: true,
			onTouch: true
			},
		scroll: {
        easing: "",
        duration: 1200
    }
	});
		});
    </script>
    <script>
	//CALL TESTIMONIAL ROTATOR
	$( function() {
		/*
		- how to call the plugin:
		$( selector ).cbpQTRotator( [options] );
		- options:
	 	{
			// default transition speed (ms)
			speed : 700,
			// default transition easing
			easing : 'ease',
			// rotator interval (ms)
			interval : 8000
		}
		- destroy:
		$( selector ).cbpQTRotator( 'destroy' );
		*/
		$( '#cbp-qtrotator' ).cbpQTRotator();
	} );
    </script>
    <script>
	//CALL PRETTY PHOTO
	$(document).ready(function(){
		$("a[data-gal^='prettyPhoto']").prettyPhoto({social_tools:'', animation_speed: 'normal' , theme: 'dark_rounded'});
	});
    </script>
    <script>
	//MASONRY
	$(document).ready(function(){
	var $container = $('#content');
	  $container.imagesLoaded( function(){
		$container.isotope({
		filter: '*',	
		animationOptions: {
		 duration: 750,
		 easing: 'linear',
		 queue: false,	 
	   }
	});
	});
	$('#filter a').click(function (event) {
		$('a.selected').removeClass('selected');
		var $this = $(this);
		$this.addClass('selected');
		var selector = $this.attr('data-filter');
		$container.isotope({
			 filter: selector
		});
		return false;
	});
	});
	//ROLL ON HOVER
	$(function() {
	$(".roll").css("opacity","0");
	$(".roll").hover(function () {
	$(this).stop().animate({
	opacity: .8
	}, "slow");
	},
	function () {
	$(this).stop().animate({
	opacity: 0
	}, "slow");
	});
	});
    </script>
    <script type="text/javascript">
    $(function () {
	  $('#nav li').removeClass('active');
	  $('#nav li:eq(0)').addClass('active');
    });
    </script>
    <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-43530398-1', '4thpointer.com');
        ga('send', 'pageview');

    </script>


<?php wp_footer(); ?>

</body>
</html>
