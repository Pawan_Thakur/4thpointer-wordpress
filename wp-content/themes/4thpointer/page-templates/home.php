<?php
/**
*
* Template Name : Home page 
*
*/
?>

<!-- CAROUSEL === -->
	<section class="carousel carousel-fade slide home-slider" id="c-slide">
	<ol class="carousel-indicators">
		<li data-target="#c-slide" data-slide-to="0" class="active"></li>
		<li data-target="#c-slide" data-slide-to="1" class=""></li>
		<li data-target="#c-slide" data-slide-to="2" class=""></li>
	</ol>
	<div class="carousel-inner">
		<div class="item active" style="background: url(img/homeSlider/slide1.jpg);">
			<div class="container">
				<div class="row">
					<div class="col-md-6 fadein scaleInv anim_1">
						<div class="fadein scaleInv anim_2">
							<h1 class="carouselText1 animated fadeInUpBig"><span class="colortext">Welcome to </span>4thPointer Services </h1>
						</div>
						<div class="fadein scaleInv anim_1">
							<p class="carouselText2 animated fadeInLeft">
                                Pointing Towards Excellence
							</p>
						</div>
						<div class="fadein scaleInv anim_2">
							<p class="carouselText3">
								<i class="icon-ok"></i>Responsive Web Development
							</p>
						</div>
						<div class="fadein scaleInv anim_3">
							<p class="carouselText3">
								<i class="icon-ok"></i>It Support for Business Solution
							</p>
						</div>
						<div class="fadein scaleInv anim_4">
							<p class="carouselText3">
								<i class="icon-ok"></i>Digital Marketing & Promotions
							</p>
						</div>
						<div class="fadein scaleInv anim_5">
							<p class="carouselText3">
								<i class="icon-ok"></i>App Development
							</p>
						</div>
					</div>
					<div class="col-md-6 text-center fadein scaleInv anim_2">
						<div class="text-center">
							<div class="fadein scaleInv anim_3">
								<img src="img/homeSlider/slide1-3.png" alt="" class="slide1-3 animated rollIn">
							</div>
							<div class="fadein scaleInv anim_8">
								<img src="img/homeSlider/slide1-1.png" alt="" class="slide1-1 animated rollIn">
							</div>
							<div class="fadein scaleInv anim_5">
								<img src="img/homeSlider/slide1-2.png" alt="" class="slide1-2 animated rollIn">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="item" style="background: url(img/homeSlider/slide2.jpg);">
			<div class="container">
				<div class="row">
					<div class="col-md-6 animated fadeInUp notransition">
						<img src="img/homeSlider/desktop3.png" alt="" style="width:90%;">
					</div>
					<div class="col-md-6 animated fadeInDown  notransition topspace30 text-right">
						<div class="car-highlight1 animated fadeInLeftBig">
							IT Solutions and Support for
						</div>
						<br>
						<div class="car-highlight2 animated fadeInRightBig notransition">
							 E-Learning Web Solutions
						</div>
						<br>
						<div class="car-highlight3 animated fadeInUpBig notransition">
							 Logistics and Transport Business
						</div>
						<br>
						<div class="car-highlight4 animated flipInX notransition">
							 Accademic and Corporate Event Solution 
						</div>
						<br>
						<div class="car-highlight5 animated rollIn notransition">
							 At 4thPointer <span class="font100">we strive everyday</span><br>
							<span class="font100" style="font-size:20px;">to bring excellence</span> to all we do<br>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="item" style="background: url(img/homeSlider/slide3.jpg);">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<br>
						<br>
						<div class="animated fadeInDownBig notransition">
							<span class="car-largetext">App Design <span style="color:#f39c12; font-weight:bold" class="font300">&amp; Development</span> Service</span><br>
						</div>
						<br>
						<br>
						<div class="car-widecircle animated fadeInLeftBig notransition">
							<span><img src="img/homeSlider/slide3-1.png" alt=""></span>
						</div>
						<div class="car-widecircle animated fadeInUpBig notransition">
							<span><img src="img/homeSlider/slide3-2.png" alt=""></span>
						</div>
						<div class="car-widecircle animated fadeInRightBig notransition">
							<span><img src="img/homeSlider/slide3-3.png" alt=""></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.carousel-inner -->
	<a class="left carousel-control animated fadeInLeft" href="#c-slide" data-slide="prev"><i class="icon-angle-left"></i></a>
	<a class="right carousel-control animated fadeInRight nxt" href="#c-slide" data-slide="next"><i class="icon-angle-right"></i></a>
	</section>
	<!-- /.carousel end-->
	<!-- /.wrapsemibox start-->
	<div class="wrapsemibox">
		<div class="semiboxshadow text-center">
			<img src="img/shp.png" class="img-responsive" alt="">
		</div>
		<!-- INTRO NOTE === -->
		<section class="intro-note topspace10">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h1>Welcome to <span class="colortext">4thpointer Services</span></h1>
					<p>
						 4thPointer Services provide a full range of web design and development services needed to direct and deliver a successful and cost effective, reliable and quality web solution of business, which enable our customers to have sustainable differential advantage over their competition.
					</p>
				</div>
			</div>
		</div>
		</section>
		<!-- /.intro-note end-->
		<!-- SERVICE BOXES === -->
		<section class="service-box topspace30">
		<div class="container">
			<div class="row">
				<div class="col-md-3 text-center animated fadeInLeftNow notransition">
					<div class="icon-box-top">
						<i class="fontawesome-icon medium circle-white center icon-desktop"></i>
						<h1>Responsive Web Designing</h1>
						<p>
							 We design websites that responds to the user's behavior and environment based on screen size, platform and orientation.
						</p>
						<p class="fontupper">
							<a href="responsive_web_designing.php" class="readmore">Read More <i class="icon-double-angle-right"></i></a>
						</p>
					</div>
				</div>
				<div class="col-md-3 text-center animated fadeInLeftNow notransition">
					<div class="icon-box-top">
						<i class="fontawesome-icon medium circle-white center icon-cogs"></i>
						<h1>Business Applications</h1>
						<p>
							 We leverages with best in class enterprise wide digital transformation of your business operations. 
						</p>
						<p class="fontupper">
							<a href="business_solution_and_support.php" class="readmore">Read More <i class="icon-double-angle-right"></i></a>
						</p>
					</div>
				</div>
				<div class="col-md-3 text-center animated fadeInRightNow notransition">
					<div class="icon-box-top">
						<i class="fontawesome-icon medium circle-white center icon-qrcode"></i>
						<h1>App Development Service</h1>
						<p>
							 We build high-performance mobile apps to increase productivity and business value in the digital marketplace. 
						</p>
						<p class="fontupper">
							<a href="app_development.php" class="readmore">Read More <i class="icon-double-angle-right"></i></a>
						</p>
					</div>
				</div>
				<div class="col-md-3 text-center animated fadeInRightNow notransition">
					<div class="icon-box-top">
						<i class="fontawesome-icon medium circle-white icon-bullhorn"></i>
						<h1>Digital Marketing</h1>
						<p>
							 Our process starts with need assessment followed by target audience identification and competitor analysis.
						</p>
						<p class="fontupper">
							<a href="digital_marketing.php" class="readmore">Read More <i class="icon-double-angle-right"></i></a>
						</p>
					</div>
				</div>
			</div>
		</div>
		</section>
		<!-- /.service-box end-->
		<!-- RECENT WORK === -->
		<section class="home-portfolio bgarea topspace30">
		<div class="bgarea-semitransparent">
			<div class="container">
				<h1 class="small text-center animated fadeInLeftNow notransition">HOW WE WORK</h1>
				<p class="animated fadeInRightNow notransition text-center topspace20">
				Simple- we work with you, for you and for your business interests. <br/>For us, your growth translates into our growth and your trust is the most important ROI for us.	</p>
				<br/>
				<div class="row">
					<div class="col-md-6 animated fadeInLeftNow notransition">
						<div class="carousel carousel-fade slide home-slider" id="carousel-featuredwork">
							<ol class="carousel-indicators">
								<li data-target="#carousel-featuredwork" data-slide-to="0" class="active"></li>
								<li data-target="#carousel-featuredwork" data-slide-to="1" class=""></li>
								<li data-target="#carousel-featuredwork" data-slide-to="2" class=""></li>
							</ol>
							<div class="carousel-inner" style="margin-top:-20px;">
								<div class="item active">
									<img src="img/howtowork1.png" alt="">
								</div>
								<div class="item">
									<img src="img/howtowork2.png" alt="">
								</div>
								<div class="item">
									<img src="img/howtowork3.png" alt="">
								</div>
							</div>
							<!-- /.carousel-inner -->
	<a class="left carousel-control animated fadeInLeft" href="#carousel-featuredwork" data-slide="prev" style="display:none;"><i class="icon-angle-left"></i></a>
	<a class="right carousel-control animated fadeInRight nxt" href="#carousel-featuredwork" data-slide="next" style="display:none;"><i class="icon-angle-right"></i></a>
							<!-- /.carousel end-->
						</div>
					</div>
					<div class="col-md-6 animated fadeInRightNow notransition">
						<ul class="icons">
							<li>
							<h4><i class="icon-headphones"></i>We listen</h4>
							<p>
								 Each project & client is unique to us and hence we take time to listen, understand, study and analyse your business processes and requirements. We conduct brain storming sessions before arriving at the steps needed to attain the set goals.
							</p>
							</li>
							<li>
							<h4><i class="icon-bar-chart"></i>We Strategies</h4>
							<p>
								 We strategize the best possible approach to deliver the solution with the highest quality and attain the set goals. We plan and allocate resources, set milestones & to-dos. We also plan backup resources to keep up to the timeline and quality.
							</p>
							</li>
							<li>
							<h4><i class="icon-puzzle-piece"></i>We build</h4>
							<p>
								 We code each and every aspect of the requirement of the project. Our standardise coding pattern always build a robust deliverable, code reusability and optimization is always consider during writing a project code.
							</p>
							</li>
							<li>
							<h4><i class="icon-shopping-cart"></i>Our deliverable</h4>
							<p>
								 We work towards delivering the solution in the agreed timeline with quality work so our clients get their true business potential. We meticulously deliver the project in stages and work as IT partner of our clients throughout the process and apply timely correctives.
							</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		</section>
		<!-- /.recent-work end-->
		<!-- FEATURES === -->
		<section class="home-features topspace30">
		<div class="container animated fadeInUpNow notransition">
			<h1 class="small text-center">WHAT THEY SAY</h1>
			<p class="text-center"> We are proud to celebrate a real 4thPointer 'Ethos'. This is built from our vision of Excellence and should be evident <br>in how we act, behave, the choices we make and the people and partners we meet.</p>
			<div class="br-hr type_short">
				<span class="br-hr-h">
				<i class="icon-pencil"></i>
				</span>
			</div>
			<div id="cbp-qtrotator" class="cbp-qtrotator">
				<div class="cbp-qtcontent">
					<img src="img/abhiskh.jpg" alt="">
					<blockquote>
						<p class="bigquote">
							<i class="icon-quote-left colortext quoteicon"></i> 4thpointer proposed us personnel of very high quality. I have personally interviewed a large number of people presented by 4thpointer, and I could perceive in all the candidates a very strong admiration for 4thpointer and an intense desire to work for this company. 4thpointer always showed a true....
						</p>
						<footer>Mr. Abhishek Bajaj / Founder of <a href="http://camistri.com" target="_blank">CAMistri.com</a></footer>
					</blockquote>
				</div>
				<div class="cbp-qtcontent">
					<img src="img/varun_gupta.jpg" alt="">
					<blockquote>
						<p class="bigquote">
							<i class="icon-quote-left colortext quoteicon"></i> 4thpointer provides the best quality deliverables even in the most strenuous times. With our aggressive deadlines, the team always emerges successfully to the challenge. In addition they communicate questions and clarifications clearly and logically....
						</p>
						<footer>Mr. Varun Gupta / Founder of <a href="http://www.rentinandout.com" target="_blank">rentinandout.com</a></footer>
					</blockquote>
				</div>
				<div class="cbp-qtcontent">
					<img src="img/rahul.jpg" alt="">
					<blockquote>
						<p class="bigquote">
							<i class="icon-quote-left colortext quoteicon"></i> 4thpointer Services showed professionalism and outstanding efficiency. Any questions and problems, arising during the development cycle, were attended to by 4thpointer specialists in a timely fashion leaving no unresolved issues....
						</p>
						<footer>Mr. Rahul Agarwal / MD of <a href="http://santushti.co.in" target="_blank">santushti.co.in</a></footer>
					</blockquote>
				</div>
			</div>
			<br/>
		</div>
		</section>
		<!-- /.home-features end-->
		<!-- /.recent-projects-home start-->
		<section class="grayarea recent-projects-home topspace30 animated fadeInUpNow notransition">
		<div class="container">
			<div class="row">
				<h1 class="small text-center topspace0">Our Clientele</h1>
				<div class="text-center smalltitle">
				</div>
				<div class="col-md-12">
					<div class="list_carousel text-center clients">
						<div class="carousel_nav">
							<a class="prev" id="car_prev" href="#"><span>prev</span></a>
							<a class="next" id="car_next" href="#"><span>next</span></a>
						</div>
						<div class="clearfix">
						</div>
						<ul id="carousel-projects">
							<li>
							<div class="boxcontainer"> 
								<img src="img/clientslogo/mylearninggraph.jpg" alt="Mylearninggraph" title="Mylearninggraph">
							</div>
							</li>
							<li>
							<div class="boxcontainer"> 
								<img src="img/clientslogo/centerdev.jpg" alt="Centerdev" title="Centerdev">
							</div>
							</li>
							<li>
							<div class="boxcontainer"> 
								<img src="img/clientslogo/exampointer.jpg" alt="Exampointer" title="Exampointer">
							</div>
							</li>
							<li>
							<li>
							<div class="boxcontainer"> 
								<img src="img/clientslogo/examzguru.png" alt="Examzguru" title="Examzguru">
							</div>
							</li>
							<li>
							<div class="boxcontainer"> 
								<img src="img/clientslogo/rocketresume.jpg" alt="Rocketresumebuilder" title="Rocketresumebuilder">
							</div>
							</li>
							
							<li>
							<div class="boxcontainer"> 
								<img src="img/clientslogo/dhl-logo-small.jpg" alt="DHL Footwears" title="DHL Footwears">
							</div>
							</li>

							<li>
							<div class="boxcontainer"> 
								<img src="img/clientslogo/wobot-logo-small.jpg" alt="Wobot" title="Wobot">
							</div>
							</li>
							<!--featured-projects 1-->
							<li>
							<div class="boxcontainer"> 
								<img src="img/clientslogo/lgcnslogo.jpg" alt="LG CNS" title="LG CNS">
							</div>
							</li>
							<!--featured-projects 2-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/payumoney.jpg" alt="PayUMoney" title="PayUMoney">
							</div>
							</li>
							<!--featured-projects 3-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/aiets.jpg" alt="Aiets" title="Aiets">
							</div>
							</li>
							<!--featured-projects 5-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/arcanemind-logo.png" alt="ArcaneMind" title="ArcaneMind">
							</div>
							</li>
							<!--featured-projects 6-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/tkws.jpg" alt="TKW's" title="TKW's">
							</div>
							<!--featured-projects 7-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/logo23.png" alt="IVS-India" title="IVS-India">
							</div>
							</li>
							<!--featured-projects 8-->
							<li> 
							<div class="boxcontainer">
								<img src="img/clientslogo/camistri.jpg" alt="CA Mistri" title="CA Mistri">
							</div>
							</li>
							<!--featured-projects 9-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/santusti.jpg" alt="Santushti" title="Santushti">
							</div>
							</li>
							<!--featured-projects 10-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/discovery.jpg" alt="Discovery Infrastructure" title="Discovery Infrastructure">
							</div>
							</li>
							<!--featured-projects 11-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/bharti.jpg" alt="Bharti Shoes" title="Bharti Shoes">
							</div>
							</li>
							<!--featured-projects 12-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/gbclogo.jpg" alt="Gupta Book Centre" title="Gupta Book Centre">
							</div>
							</li>
							<!--featured-projects 13-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/dni.png" alt="DnI-Institute" title="DnI-Institute">
							</div>
							</li>
							<!--featured-projects 14-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/motionlogo.jpg" alt="Motion Trumpet" title="Motion Trumpet">
							</div>
							</li>
							<!--featured-projects 16-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/geekspointer.jpg" alt="Geekspointer" title="Geekspointer">
							</div>
							</li>
							<!--featured-projects 17-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/ravicreation.jpg" alt="Ravi Creations" title="Ravi Creations">
							</div>
							</li>
							<!--featured-projects 18-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/kidsmindtree.jpg" alt="kids Mind Tree" title="kids Mind Tree">
							</div>
							<!--featured-projects 19-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/salfialogo.jpg" alt="Salfia" title="Salfia">
							</div>
							</li>
							<!--featured-projects 21-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/pja_log1.png" alt="Phalodi" title="Phalodi">
							</div>
							</li>
							<!--featured-projects 22-->
							<li>
							<div class="boxcontainer"> 
								<img src="img/clientslogo/rbil.png" alt="RBIPL" title="RBIPL">
							</div>
							</li>
							<!--featured-projects 23-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/saving.png" alt="SAVING-MONK" title="SAVING-MONK">
							</div>
							</li>
							<!--featured-projects 24-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/rentinandout.png" alt="Rentin&out" title="Rentin&out">
							</div>
							</li>
							<!--featured-projects 25-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/achieverz.png" alt="achieverz" title="achieverz">
							</div>
							</li>
							<!--featured-projects 26-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/saran.png" alt="saran" title="SARAN">
							</div>
							</li>
							<!--featured-projects 27-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/DNICONSULTING.png" alt="DNICONSULTING" title="DNIConsulting">
							</div>
							</li>
							<!--featured-projects 28-->
							<li>
							<div class="boxcontainer">
								<img src="img/clientslogo/stanford.jpg" alt="stanford Park" title="Stanford Park">
							</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		</section>
		<!--CALL TO ACTION PANEL == -->
		<section class="container animated fadeInDownNow notransition topspace40">
		<div class="row">
			<div class="col-md-12">
				<div class="text-center">
					<p class="bigtext">
						 Click <a href="contact.php"><span class="fontpacifico colortext">Here</span></a> To Get A Free Quote
					</p>
					
				</div>
			</div>
		</div>
		</section>
		<!-- /. end call to action-->
	</div>
	
<?php wp_footer(); ?>