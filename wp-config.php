<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '4thpointer');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Gbsk UQwa%z,tr]t.vM)C ] [wUk?&ET=N:luPuU>lA2yV,}oC^=5H ]#hn|mkVp');
define('SECURE_AUTH_KEY',  'KF9/X#w/#(VTtX*o}`%}$^ESc%kjR==A;J&lt@!+$P@]t`y2.r[WpK![TaNUK0%a');
define('LOGGED_IN_KEY',    'kjsRpoVz2>P|~uKQfMNS3(z2Z$e[KfCeo.3IlcW_pr1qjQ^jwM8TC/IqNOV<27LJ');
define('NONCE_KEY',        'lJcQ)5MgV>c5%Kxa2;DiIm|$y$5+w%bZ^aP11S[+BU*])>{1H8ri*@zzXOA2H44x');
define('AUTH_SALT',        ',Rw0.L-/sHUhbYc(a_(mdzbai0uJ<^L;Ihy}Yy]- (#sDbu~~HkaE_cEi[~Mr8_+');
define('SECURE_AUTH_SALT', '|1.S6wkOoQp6GtC?aWUxRz=/EP#<w~IX4]/vXool=gTw6fO1r_#/&jr@+^wQ=K/m');
define('LOGGED_IN_SALT',   'Nd^bt56:4B5;ZY Xya%T>[9i-$i:5vuM(9C8Pz{6-_ 6jQL$Z2J;MM?w!^{hCv?H');
define('NONCE_SALT',       'Oz-.VYS@G8%d;KT8]^3)s0rAQ<`7gjH+5~YrC>#XeGEo!EXQA$4PtB8VB,PIW$CE');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
